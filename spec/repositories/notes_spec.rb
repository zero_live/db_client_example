require_relative '../../system/repositories/notes'

describe 'Notes repository' do
  before(:each) do
    Repositories::Notes._flush
  end

  after(:each) do
    Repositories::Notes._flush
  end

  it 'stores a note' do
    note = { 'id' => 0, 'text' => 'Note text', 'done_at' => '' }

    Repositories::Notes.store(note['text'])

    stored_note = Repositories::Notes.all.first
    expect(stored_note).to eq(note)
  end

  it 'brings all the notes' do
    Repositories::Notes.store('Note text')
    Repositories::Notes.store('Note text')

    stored_notes = Repositories::Notes.all

    expect(stored_notes.count).to eq(2)
  end

  it 'finds notes by id' do
    note = { 'id' => 0, 'text' => 'Note text', 'done_at' => '' }
    Repositories::Notes.store(note['text'])

    finded_note = Repositories::Notes.find(note['id'])

    expect(finded_note).to eq(note)
  end

  it 'updates a note' do
    note = { 'id' => 0, 'text' => 'Note text', 'done_at' => '' }
    Repositories::Notes.store(note['text'])
    new_text = 'New text'
    done_at = 'someday'

    Repositories::Notes.update(note['id'], new_text, done_at)

    updated_note = Repositories::Notes.find(note['id'])
    expect(updated_note).to eq({ 'id' => note['id'], 'text' => new_text, 'done_at' => done_at })
  end

  it 'deletes a note' do
    Repositories::Notes.store('Anything')
    id = 0

    Repositories::Notes.delete(id)

    notes = Repositories::Notes.all
    expect(notes.count).to eq(0)
  end
end
