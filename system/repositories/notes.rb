require_relative '../infrastructure/mongo_client'

module Repositories
  class Notes
    class << self
      def store(text_note)
        client.insert_one(text_note)

        nil
      end

      def all
        client.find_all
      end

      def find(id)
        client.find_one(id)
      end

      def update(id, text='', done_at='')
        client.update_one(id, text, done_at)

        nil
      end

      def delete(id)
        client.delete_one(id)

        nil
      end

      def _flush
        client._flush

        nil
      end

      private

      def client
        Infrastructure::MongoClient
      end
    end
  end
end
