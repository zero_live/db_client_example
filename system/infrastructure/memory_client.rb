
module Infrastructure
  class MemoryClient
    class << self
      def insert_one(text)
        element = {
          'id' => new_id,
          'text' => text,
          'done_at' => ''
        }
        collection << element

        nil
      end

      def find_all
        collection.dup
      end

      def find_one(id)
        element = find(id)

        element.dup
      end

      def update_one(id, text='', done_at='')
        element = find(id)

        element['text'] = text
        element['done_at'] = done_at

        nil
      end

      def delete_one(id)
        collection.delete_if do |element|
          element['id'] == id
        end

        nil
      end

      def _flush
        @notes = []

        nil
      end

      private

      def new_id
        collection.size
      end

      def find(id)
        element = collection.find do |element|
          element['id'] == id
        end

        element
      end

      def collection
        @notes ||= []
      end
    end
  end
end
