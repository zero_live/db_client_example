require 'mongo'

module Infrastructure
  class MongoClient
    class << self
      URI = "mongodb://mongodb:27017/db"

      def insert_one(text)
        document = {
          'id' => new_id,
          'text' => text,
          'done_at' => ''
        }
        collection.insert_one(document)
        client.close

        nil
      end

      def find_all
        documents = collection.find({})
        client.close

        documents.map do |document|
          document.delete('_id')
          document
        end
      end

      def find_one(id)
        query = { 'id' => id }
        documents = collection.find(id: id)
        client.close

        document = documents.first
        document.delete("_id")
        document
      end

      def update_one(id, text='', done_at='')
        collection.update_one(
          { id: id },
          '$set' => { 'text' => text, 'done_at' => done_at }
        )
        client.close

        nil
      end

      def delete_one(id)
        query = { 'id' => id }
        document = collection.delete_one(id: id)
        client.close

        nil
      end

      def _flush
        collection.drop
        client.close

        nil
      end

      private

      def new_id
        id = collection.count({})
        client.close

        id
      end

      def collection
        client[:notes]
      end

      def client
        Mongo::Logger.logger.level = Logger::INFO

        @client ||= Mongo::Client.new(URI)
        @client
      end
    end
  end
end
