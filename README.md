#BD Client example for Devscola

It's only a prepared code for show at Devscola how to use a dbclient.

#System requirements

- Docker version 17.12.0-ce or higher.
- docker-compose version 1.18.0 or higher.

#Development

##Running the project

You have to run in the project folder the following command:

- `docker-compose up --build`

##Running tests

You have to have the project running and after that in the project folder run the following command:

- `docker-compose run --rm app rspec`
