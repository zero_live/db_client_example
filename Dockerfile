FROM ruby:2.4.2

WORKDIR /app
ADD . /app

RUN gem install bundle
RUN bundle install
